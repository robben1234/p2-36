<?php
/**
 * Created by PhpStorm.
 * User: robben1
 * Date: 26.10.15
 * Time: 18:10
 */


include "form.html.php";

if (isset($_SET['file'])) {
    include $_SERVER['DOCUMENT_ROOT'] . "/includes/upload.inc.php";
}


if (isset($_POST["filename"])) {
    try {
        if (file_exists($_POST['filename'])) {
            $filename = "./" . $_POST["filename"];
            $file = fopen($filename, 'r+');
        }
        else {
            $error = 'File does not exists.';
            throw new Exception($error);
        }

    }
    catch (Exception $e) {
        $error = "Ошибка связанная с файлом: " . $e->getMessage();
        include 'error.html.php';
        exit();
    }

    $main = fread($file, filesize($_POST['filename']));

    if (isset($_POST['start'])) {
        $start = @substr($main, 0, $_POST['start']);
        $main = str_replace($start, "", $main);
    }
    //else - с нуля

    if (isset($_POST['end'])) {
        @fseek($file, $_POST['end']);
        $end = fread($file, filesize($_POST['filename']));
        $main = str_replace($end, "", $main);

    }
    //else - до конца
    fclose($file);
    $file = fopen($_POST['filename'], 'a+');

    $stats = fstat($file)['atime'];
    $stats = date('m/d/Y H:i:s', $stats);

    include $_SERVER['DOCUMENT_ROOT'] . '/includes/fullpermsfile.inc.php'; //ПРЕОБРАЗОВАНИЕ DECOCT числа в unix представление
    //$info - строка с правами

    $stringToWrite = $stats . " " . $info;
    fwrite($file, $stringToWrite);



    $output = $main;
    include 'output.html.php';
}