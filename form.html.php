<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Form</title>
    <style>
        label {
            display: block;
            clear: left;
        }
        input {
            width: 30%;
            margin: 5px;
        }
    </style>
</head>
<body>
    <form action="?" method="post">
        <div>
            <label for="filename">Name of file:</label>
            <input type="text" name="filename" id="filename" placeholder="name of file"/>
            <label for="start">Start point:</label>
            <input type="text" name="start" id="start" placeholder="bytes | leave empty for 0"/>
            <label for="end">End point:</label>
            <input type="text" name="end" id="end" placeholder="bytes | leave empty for EOF"/>
        </div>
        <div>
            <input type="submit" value="Submit"/>
        </div>
    </form>

<form enctype="multipart/form-data" action="?" method="post">
    <div>
        <input type="hidden" name="MAX_FILE_SIZE" value="30000"/>
        <input type="file" name="file" id="file"/>
    </div>
    <div>
        <input type="submit" value="Submit file"/>
    </div>
</form>

</body>
</html>